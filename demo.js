function checkDate(strInputDate) {

    // 定义一个月份天数常量数组

    var DA = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    // 统一日期格式

    strDate = strInputDate.replace(/-/g, "/");

    //判断日期是否是预期的格式

    if (strDate.indexOf("/") == -1) {
        return false;

    }

    // 分解出年月日

    arrD = strDate.split("/");

    if (arrD.length != 3) return false;

    y = parseInt(arrD[0], 10);

    m = parseInt(arrD[1], 10);

    d = parseInt(arrD[2], 10);

    //判断年月日是否是数字

    if (isNaN(y) || isNaN(m) || isNaN(d)) return false;

    // 判断月份是否在1-12之间

    if (m > 12 || m < 1) return false;

    //判断是否是闰年

    if (isLoopYear(y)) DA[2] = 29;

    //判断输入的日是否超过了当月月份的总天数。

    if (d > DA[m]) return false;

    //各种条件都验证了，则应该是一个合法的日期了。

    // 如果要对日期进行一次格式化，则可以在这里进行处理了，下面格式化成数据库识别的日期格式 yyyy-MM-dd

    // str = y + "-" + (m<10?"0":"") + m + "-" + (d<10?"0":"") + d;

    str = y + "-" + (m < 10 ? "0" : "") + m + "-" + (d < 10 ? "0" : "") + d;

    return true;

}

function isLoopYear(theYear) {

    return (new Date(theYear, 1, 29).getDate() == 29);
}
console.log(checkDate('2022/02/28'));