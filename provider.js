async function scheduleHtmlProvider() {

    // 选择模块，用户让用户多选一，暂不支持多选，返回值为选项列表中某一项
    await loadTool('AIScheduleTools')
    if (document.URL.indexOf('jwapp') === -1) {
        await AIScheduleAlert({
            titleText: '当前页面不符合要求', // 标题内容，字体比较大，不传默认为提示
            contentText: '请前往的课表或者成绩查询界面完成导入操作', // 提示信息，字体稍小，支持使用``达到换行效果，具体使用效果建议真机测试
            confirmText: '确认', // 确认按钮文字，可不传默认为确认
          })
        return 'do not continue'
      }

    try {

        let thisYear = new Date().getFullYear();
        let term = await AIScheduleSelect({
            titleText: '选择导入的学期', // 标题内容，字体比较大，超过10个字不给显示的喔，也可以不传就不显示
            contentText: '从下面四个选项中选择导入的学期', // 提示信息，字体稍小，支持使用``达到换行效果，具体使用效果建议真机测试，为必传，不传显示版本号
            selectList: [
                `${thisYear - 1}-${thisYear}年-第1学期`,
                `${thisYear - 1}-${thisYear}年-第2学期`,
                `${thisYear}-${thisYear + 1}年-第1学期`,
                `${thisYear}-${thisYear + 1}年-第2学期`,
            ], // 选项列表，数组，为必传
        })
        term = term.replace(/[\u4e00-\u9fa5]/g, "")
        let response = await fetch("http://ehallapp.nnu.edu.cn/jwapp/sys/wdkb/modules/xskcb/cxxszhxqkb.do", {
            "headers": {
                "accept": "application/json, text/javascript, */*; q=0.01",
                "accept-language": "zh-CN,zh;q=0.9",
                "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                "x-requested-with": "XMLHttpRequest"
            },
            "referrer": "http://ehallapp.nnu.edu.cn/jwapp/sys/wdkb/*default/index.do?t_s=1645685546835&amp_sec_version_=1&gid_=YXRpaDloek1OYmhLcHZRcFRXUUM3NDBvRFNQKzhoekRzRDZVUlJyWUpqVlowNEhlZTFmaVg1WmhBdDV6dTV6SytwV3RrTHoyb3J6US9DQm5qQnBmRGc9PQ&EMAP_LANG=zh&THEME=teal",
            "referrerPolicy": "strict-origin-when-cross-origin",
            "body": `XNXQDM=${term}&*order=%2BKSJC`,
            "method": "POST",
            "mode": "cors",
            "credentials": "include"
        });;
        let courseData = await response.json();
        return JSON.stringify(courseData.datas.cxxszhxqkb);
    } catch (error) {
        console.log(error);
        await AIScheduleAlert({
            titleText: '当前页面不符合要求', // 标题内容，字体比较大，不传默认为提示
            contentText: '请前往的课表或者成绩查询界面完成导入操作', // 提示信息，字体稍小，支持使用``达到换行效果，具体使用效果建议真机测试
            confirmText: '确认', // 确认按钮文字，可不传默认为确认
        })
        return 'do not continue'
    }

}