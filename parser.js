function scheduleHtmlParser(data) {
  let result = [];
  let courses = JSON.parse(data).rows;
  for (let course of courses) {
    let courseInfo = {
      weeks: [],
      sections: []
    };
    courseInfo.name = course.KCM;
    courseInfo.weeks = parseWeek(course.ZCMC);
    for (let i = course.KSJC; i <= course.JSJC; i++) {
      courseInfo.sections.push(i);
    }
    courseInfo.teacher = (course.SKJS == '' || course.SKJS == null) ? '无' : course.SKJS;
    courseInfo.position = (course.JASMC==''||course.JASMC==null)?'无':course.JASMC;
    courseInfo.day = course.SKXQ;
    result.push(courseInfo);
  }
  return result;
}

function parseWeek(str) {
  let weeks = []
  let weekParts = str.replace(/[\u4e00-\u9fa5]/g, "").split(/[,，]/);
  for (let weekPart of weekParts) {
    if (weekPart.indexOf('-') == -1) {
      weeks.push(parseInt(weekPart));
    } else {
      let [start, end,] = weekPart.split('-');
      for (let i = parseInt(start); i <= parseInt(end); i++)
        weeks.push(i);
    }
  }
  return weeks;
}
