/**
 * 时间配置函数，此为入口函数，不要改动函数名
 */
async function scheduleTimer({
    providerRes,
    parserRes
} = {}) {
    await loadTool('AIScheduleTools')
    let maxWeek = parserRes.reduce((max, course) => {
        return Math.max(...course.weeks) > max ? Math.max(...course.weeks) : max;
    }, 0)
    // 获取当前日期
    var date = new Date();

    // 获取当前月份
    var nowMonth = date.getMonth() + 1;

    // 获取当前是几号
    var strDate = date.getDate();

    // 添加分隔符“-”
    var seperator = "-";

    // 对月份进行处理，1-9月在前面添加一个“0”
    if (nowMonth >= 1 && nowMonth <= 9) {
        nowMonth = "0" + nowMonth;
    }

    // 对月份进行处理，1-9号在前面添加一个“0”
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }

    // 最后拼接字符串，得到一个格式为(yyyy-MM-dd)的日期
    var nowDate = date.getFullYear() + seperator + nowMonth + seperator + strDate;
    const sdate = await AISchedulePrompt({
        titleText: '开学日期', // 标题内容，字体比较大，超过10个字不给显示的喔，也可以不传就不显示
        tipText: '请填写开学日期（格式例如2021-05-10，2022/2/2）', // 提示信息，字体稍小，支持使用``达到换行效果，具体使用效果建议真机测试，也可以不传就不显示
        defaultText: nowDate, // 文字输入框的默认内容，不传会显示版本号，所以空内容要传个''
        validator: value => { // 校验函数，如果结果不符预期就返回字符串，会显示在屏幕上，符合就返回false
            if (!checkDate(value)) return '日期非法'
            return false
        }
    })
    // 返回时间配置JSON，所有项都为可选项，如果不进行时间配置，请返回空对象
    return {
        totalWeek: maxWeek, // 总周数：[1, 30]之间的整数
        startSemester: (+new Date(sdate)).toString(), // 开学时间：时间戳，13位长度字符串，推荐用代码生成
        startWithSunday: false, // 是否是周日为起始日，该选项为true时，会开启显示周末选项
        showWeekend: false, // 是否显示周末
        forenoon: 5, // 上午课程节数：[1, 10]之间的整数
        afternoon: 4, // 下午课程节数：[0, 10]之间的整数
        night: 3, // 晚间课程节数：[0, 10]之间的整数
        sections: [{
            section: 1, // 节次：[1, 30]之间的整数
            startTime: '08:00', // 开始时间：参照这个标准格式5位长度字符串
            endTime: '08:40', // 结束时间：同上
        },
        {
            section: 2,
            startTime: '08:45',
            endTime: '09:25'
        },
        {
            section: 3,
            startTime: '09:40',
            endTime: '10:20'
        },
        {
            section: 4,
            startTime: '10:35',
            endTime: '11:15'
        },
        {
            section: 5,
            startTime: '11:20',
            endTime: '12:00'
        },
        {
            section: 6,
            startTime: '13:30',
            endTime: '14:10'
        },
        {
            section: 7,
            startTime: '14:15',
            endTime: '14:55'
        },
        {
            section: 8,
            startTime: '15:10',
            endTime: '15:50'
        },
        {
            section: 9,
            startTime: '15:55',
            endTime: '16:35'
        },
        {
            section: 10,
            startTime: '18:30',
            endTime: '19:10'
        },
        {
            section: 11,
            startTime: '19:20',
            endTime: '20:00'
        },
        {
            section: 12,
            startTime: '20:10',
            endTime: '20:50'
        }], // 课程时间表，注意：总长度要和上边配置的节数加和对齐
    }
    // PS: 夏令时什么的还是让用户在夏令时的时候重新导入一遍吧，在这个函数里边适配吧！奥里给！————不愿意透露姓名的嘤某人
}

function checkDate(strInputDate) {

    // 定义一个月份天数常量数组

    var DA = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    // 统一日期格式

    strDate = strInputDate.replace(/-/g, "/");

    //判断日期是否是预期的格式

    if (strDate.indexOf("/") == -1) {
        return false;

    }

    // 分解出年月日

    arrD = strDate.split("/");

    if (arrD.length != 3) return false;

    y = parseInt(arrD[0], 10);

    m = parseInt(arrD[1], 10);

    d = parseInt(arrD[2], 10);

    //判断年月日是否是数字

    if (isNaN(y) || isNaN(m) || isNaN(d)) return false;

    // 判断月份是否在1-12之间

    if (m > 12 || m < 1) return false;

    //判断是否是闰年

    if (isLoopYear(y)) DA[2] = 29;

    //判断输入的日是否超过了当月月份的总天数。

    if (d > DA[m]) return false;

    //各种条件都验证了，则应该是一个合法的日期了。

    // 如果要对日期进行一次格式化，则可以在这里进行处理了，下面格式化成数据库识别的日期格式 yyyy-MM-dd

    // str = y + "-" + (m<10?"0":"") + m + "-" + (d<10?"0":"") + d;

    str = y + "-" + (m < 10 ? "0" : "") + m + "-" + (d < 10 ? "0" : "") + d;

    return true;

}

function isLoopYear(theYear) {

    return (new Date(theYear, 1, 29).getDate() == 29);
}